---
layout: 2020/post
section: propuestas
category: devrooms
title: [NAME OF THE ROOM]
---

Small introduction and motivation of the same. Please note that the idea of the room is to carry out a semi-autonomous activity within esLibre, which will normally have its own call for contributions, its own organizers, its own schedule (coordinated with that of esLibre), etc.

## Community or group that proposes it

Description of the group that has proposed it, with a link to its website and other organized activities. Identify also the people who will be in charge of it.

The theme must be in any case focused on free software, culture and hardware and open data.

## Target audience

Who should Attend?

## Organizers

Who is organizing the devroom? Which profile the have? What previous experience they have?

### Contact(s)

* Name: contact

For "Name", use the full name. For "contact", use an email address (format "user @ domain"), or the GitLab user name (format "user @ GitLab). In any case, have into account that these addresses will be used for contacting you, so you better check them frequently ;-)

## Time

Say if it's going to be done for a day or for half a day.

## Day

Indicate if you prefer to schedule the *devroom* on the first or second day.

## Format

Format of the devroom and how the call for contributions and the call for participation will be made The format is left entirely open to the proposers, and may include (but not be limited to)

* Workshops
* Lightning talks
* Plenary talks
* Round tables
* An all-day hackathon

In order for everyone to be able to organize their trips, it is necessary for the program to be complete at least one month before the Congress, with the speakers who can participate already confirmed.

## Observations

Any other relevant observations.

## Conditions

* [ ] &nbsp;I agree to follow the [code of conduct](https://eslib.re/conducta/) and request this acceptance from the attendees and speakers.
* [ ] &nbsp;At least one person among those proposing will be present on the day scheduled for the *devroom*.
* [ ] &nbsp;I agree to coordinate with the organization of esLibre.
* [ ] &nbsp;I understand that if there is no finished program by the date set by the organization, the *devroom* may be withdrawn.
