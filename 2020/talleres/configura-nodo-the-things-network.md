---
layout: 2020/post
section: propuestas
category: workshops
title: Configura tu nodo IoT y conéctalo a The Things Network
state: confirmed
---

En este taller intentamos acercar la tecnología IoT a la ciudadanía para que se sumen a la iniciativa de desplegar una red abierta, libre, neutral, segura y útil en régimen de procomún.

El taller consiste en programar con Arduino un nodo, capaz de comportarse como sensor (puerta abierta, temperatura, presión, acelerómetro...) y actuador (buzzer, LED, relé...), y darlo de alta en la red The Things Netowrk para integrarlo en un dashboard y controlarlo desde la web o el móvil.

## Objetivos a cubrir en el taller

-   Dar a conocer las capacidades de la tecnología LoRaWAN (largo alcance y bajo consumo).
-   Evidenciar la sencillez de uso de la red The Things Network.
-   Mostrar integraciones entre diversas tecnologías.

## Público objetivo

Público en general; los conceptos expuestos son fácilmente accesibles precisamente porque es una red enfocada al despliegue ciudadano, que pretende empoderar al individuo y acercarle a la tecnología. No se requieren conocimientos de programación ni electrónica.

## Ponente(s)

-   **Juan Félix Mateos Barrado**
    -   Iniciador de la comunidad The Things Network Madrid
    -   Formador tecnológico del profesorado no universitario de la Comunidad de Madrid
    -   Profesor de asignaturas de IoT, AI, prototipado electrónico... en másters de la Universidad de Alcalá de Henares
    -   He presentado la red The Things Network en diversos actos como:
        -   <https://www.lanavemadrid.com/event/lorawan-practico-monta-y-configura-tu-propio-nodo/>
        -   <https://youtu.be/EYcq3XEq_2c>
        -   <https://oshwdem.org/oshwdem_ttn2/>

### Contacto(s)

-   **Juan Félix Mateos Barrado**: juanfelixmateos at gmail dot com

## Prerrequisitos para los asistentes

Los asistentes deberán disponer de un ordenador con conexión a Internet en el que se recomienda tener instalado el IDE de Arduino (<https://www.arduino.cc/en/main/software>).

## Prerrequisitos para la organización

Si cada asistente trae su propio ordenador portátil, solo requeriríamos un proyector o pantalla para exponer la presentación.

## Tiempo

Aproximadamente 3 horas.

## Día

Nos es indiferente.

## Comentarios

Hemos propuesto también una charla de 25 minutos introducción a la iniciativa The Things Network. Convendría que la charla y el taller fuesen el mismo día.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
