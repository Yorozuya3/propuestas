---
layout: 2020/post
section: propuestas
category: workshops
title: Desarrollo incremental con "behavioral programming"&#58 una nueva forma de pensar sobre el software
state: confirmed
---

¿Te imaginas que los escenarios de comportamiento (requisitos, casos de uso, etc.) y el código de tu programa pudieran estar alineados?, o incluso, ¿que pudieran ser lo mismo?. Esta es la idea que propone el paradigma de ["behavioral programming"](http://www.wisdom.weizmann.ac.il/~bprogram/), siguiendo [la propuesta](http://www.wisdom.weizmann.ac.il/~amarron/BP%20-%20CACM%20-%20Author%20version.pdf) de David Harel y otro colaboradores.

Para los ejercicios y demostraciones prácticas se empleará el entorno de programación open source [Pharo Smalltalk](https://pharo.org) y la biblioteca de behavioral programming que los autores han desarrollado para Pharo (BP4Pharo).

## Objetivos a cubrir en el taller

El objetivo de este taller es introducir los conceptos de "behavioral programming" por medio de diferentes ejemplos prácticos que permitan a los participantes comprender el potencial de este innovador enfoque del desarrollo de software.

Los ejemplos estarán destinados a demostrar cómo estos conceptos pueden aplicarse en el desarrollo de cualquier sistema reactivo para conseguir ventajas únicas como:

-   Añadir y modificar el comportamiento de una aplicación sin necesidad de cambiar directamente, incluso ni leer o comprender, el código existente.
-   Modularizar el software de manera más natural para el desarrollador o el usuario, al alinearse directamente con los requisitos de comportamiento.
-   Desarrollar un sistema en el que nuevos módulos pueden ser añadidos, modificados o eliminados de manera muy flexible para crear nuevas versiones del sistema.

## Público objetivo

Cualquier persona con conocimientos generales de programación podrá realizar los ejercicios y comprender el alcance de este nuevo paradigma.

## Ponente(s)

-   **Rafael Luque**
-   **Francisco Javier Luque**

### Contacto(s)

-   **Rafael Luque**: rafael.luque at osoco dot es
-   **Francisco Javier Luque**: javier.luque at osoco dot es

Desarrolladores de software en [OSOCO](https://osoco.es) y miembros de [Blue Plane](https://blueplane.xyz), un grupo de investigación sin ánimo de lucro, cuya misión es incubar proyectos de I+D con la intención de conseguir un cambio de paradigma que redefina nuestra forma de pensar y abordar el desarrollo de software.

Ambos son miembros y contribuidores de la comunidad Pharo Smalltalk.

## Prerrequisitos para los asistentes

-   No serán necesarios conocimientos avanzados de programación, ni conocimientos previos de Pharo, para poder obtener provecho del taller.

-   Los ejemplos del taller podrán seguirse en cualquier equipo dotado que cuente con conexión a Internet. A ser posible con un sistema operativo libre.

## Prerrequisitos para la organización

-   Proyector o monitor para mostrar slides y código a los participantes.
-   Conexión a Internet con suficiente capacidad para que los asistentes puedan descargar el repositorio de código.

## Tiempo

Aproximadamente 2 horas.

## Día

Indiferente.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
