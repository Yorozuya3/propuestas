---
layout: 2020/post
section: propuestas
category: talks
title: Iniciación e instalación de postmarketOS
state: confirmed
---

postmarketOS es un sistema operativo GNU/Linux para móviles y tablets para sustituir a Android basado en Alpine Linux que cuenta con más de 200 dispositivos portados a diferentes niveles. Su sistema permite no solo crear una imagen ya portada, si no crear un port tú mismo con unos pasos fáciles y un proceso automatizado.

Mi motivación es que esta información sea lo más clara y exportable para cualquier usuario de GNU/Linux que hable sólo español, ya que hay muy poca información sobre este sistema en español, los grupos y la web así como la wiki está todo en ingles, y creo que es necesario exportar estos conocimientos a todo el mundo de habla hispana.

## Formato de la propuesta

Indicar uno de estos:

* [X] &nbsp;Charla (25 minutos)
* [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

La charla es una introducción a postmarketOS, qué permite hacer y qué no, los diferentes métodos de cómo se instala su aplicación (pmbootstrap) y cómo se crea una imagen ya predefinida de alguno de los 200 dispositivos compatibles.

También la sintaxis básica de instalación de aplicaciones en el sistema, actualización y configuración, esto entra más en conocimientos de sobre Alpine Linux, que es el sistema operativo bajo el que funciona pmOS.

## Público objetivo

A personas que quieran aprender sobre este sistema, personas que quieran liberar su viejo móvil sin usar en un cajón, curiosos que están cansados de Lineage y quieren algo más libre, programadores que quieran mejorar el sistema.

## Ponente(s)

-   **Pietre**:
  - [https://wiki.ingobernable.net/doku.php?id=linux-kernel](https://wiki.ingobernable.net/doku.php?id=linux-kernel)
  - [https://www.mibqyyo.com/topic/49686/manual-para-instalar-ubuntu-en-tesla-2-w8/62](https://www.mibqyyo.com/topic/49686/manual-para-instalar-ubuntu-en-tesla-2-w8/62)
  - [https://elbinario.net/2020/08/10/postmarketos-el-fin-del-principio-para-android](https://elbinario.net/2020/08/10/postmarketos-el-fin-del-principio-para-android)
  - [https://fotos.miarroba.com/pietrelinux/4-postmarketos](https://fotos.miarroba.com/pietrelinux/4-postmarketos)
  - [https://androidpc.es/tablex-una-tablet-linux-desarrollada-desde-espana](https://androidpc.es/tablex-una-tablet-linux-desarrollada-desde-espana)

### Contacto(s)

-   **Pietre**: pietre.linux at gmail dot com

## Comentarios

Todo lo que construyáis sin nosotros será destruido.

## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
