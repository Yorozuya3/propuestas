---
layout: 2020/post
section: propuestas
category: talks
title: Hackeando la comunicación: consejos para evitar conflictos y collaborar en comunidades abiertas
state: confirmed
---

Consejos para evitar conflictos y collaborar en comunidades abiertas.

## Formato de la propuesta

Indicar uno de estos:

* [X] &nbsp;Charla (25 minutos)
* [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

¿Se te hace difícil dar o recibir crítica? ¿Has tenido problemas trabajando con personas que no has conocido en persona, o que sean de una cultura diferente a la tuya?

Después de leer muchos libros de autoayuda, ver varios TED Talks y escuchar muchos podcasts, he condensado mis aprendizajes para ayudarte a mejorar tus habilidades de comunicación. Esta charla te ayudará a evitar conflictos y colaborar mejor que nunca en comunidades de código abierto (y hasta en tu vida personal).

## Público objetivo

Esta charla está dirigida a cualquiera persona que sea parte de una comunidad de software de código abierto. Los consejos que aprenderás te ayudarán tanto en tu vida profesional como en tu vida personal.

## Ponente(s)

-   **Nuritzi Sanchez**, Sr. Open Source Program Manager at GitLab, Inc.
-   Charlas en GNOME, KDE, GitLab Commit, Campus Party, etc. Ejemplo: [https://www.youtube.com/watch?v=4f4Za-X9CJQ](https://www.youtube.com/watch?v=4f4Za-X9CJQ)

### Contacto(s)

-   **Nuritzi Sanchez**, @nuritzi, nsanchez @ gitlab.com

## Comentarios

Esta charla será adaptada de inglés a español. Sugerencias para el título o cualquiera otra cosa son bienvenidas. Mi preferencia para dar la charla si soy elegida sería para el viernes. Estaré en Alemania durante la conferencia.

## Condiciones

* [X] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
