---
layout: 2020/post
section: propuestas
category: talks
title: Software libre y soberanía tecnológica en la ONG Ingeniería Sin Fronteras
state: confirmed
---

En la federación de [ONGDs](https://es.wikipedia.org/wiki/Organizaci%C3%B3n_no_gubernamental_para_el_desarrollo) Ingeniería Sin Fronteras apostamos y apoyamos el software libre. Queremos aportar nuestra experiencia al evento para mostrar como trabajamos en este área.

## Formato de la propuesta

Indicar uno de estos:

* [x] &nbsp;Charla (25 minutos)
* [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

ISF es una federaciónd e ONGDs situadas en varias comunidades del [territorio nacional](https://www.isf.es/#seccion-mapa). Somos una organización apartidista pero fuertemente política lo que supone que tengamos posicionemos. Entendemos el software libre como una herramienta transversal cuyos ideales compartimos y defendemos.

El uso de herramientas de código abierto aporta mucho valor a nuestra entidad y en general a nuestra sociedad. Las herramientas que utilizamos nos son útiles para llevar a cabo los proyectos de desarrollo en [ámbitos diversos](https://www.isf.es/que-hacemos/) como pueden ser el derecho humano al agua, la energía o la educación por el desarrollo.

El objetivo de esta charla es compartir como trabajamos entorno al software libre. Por un lado dentro del grupo de Gestión de Sistemas de Información (GSI), donde damos soporte a las herramientas que usamos (jitsi, moodle, wordpress, etc). Por otra parte, el grupo de Soberanía Tecnológica trabaja entorno a los derechos digitales. Trabajamos de forma distribuida compartimos artículos y generando debates internos sobre temas de actualidad. Algunos temas que preocupan son la neutralidad de la red, la privacidad de los datos y en general aquellos aspectos que supongan la vulneración (directa o indirecta) de derechos fundamentales.

## Público objetivo

Personas usuarias de software libre podrán conocer las herramientas que usamos. De cara a otras entidades podrán además conocer como trabajamos en pro de la soberanía tecnológica.

## Ponente(s)

La ponencia estará a cargo de Sergio Soto, actualmente voluntario en [ISF Andalucía](http://andalucia.isf.es).

Durante los últimos años ha participado como ponente y organizador en actividades para sensibilizar acerca del uso de redes sociales o el [peligro de la industria de los datos](https://andalucia.isf.es/actividad/escuela-de-activistas-que-hay-detras-de-google-la-industria-de-la-informacion-cordoba/).

Durante este año, a raíz de la epidemia COVID-2019, participó como ponente y organizador en el ciclo de charlas online *De fronteras y pandemias* de [ISF Baleares](http://baleares.isf.es) con la tertulia [Tecnología y datos, ¿control o progreso?](https://www.youtube.com/watch?v=xun1xNrTYKE)

### Contacto(s)

* Sergio Soto: sergio.soto (at) isf.es
* Alvaro López: alvaro.lopez (at) isf.es

## Comentarios


## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
