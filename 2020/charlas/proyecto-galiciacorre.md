---
layout: 2020/post
section: propuestas
category: talks
title: Crear un proyecto como GaliciaCorre con herramientas libres
state: cancelled
---

Explicación del un proyecto desarrollado por GaliciaCorre usando herramientas libres.

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

Si tenemos una idea de sitio/aplicación web, no tenemos porque recurrir a herramientas privativas. Podemos construir un proyecto con software/herramientas libres:

-   SO
-   Servidor web
-   API REST
-   Vista web
-   Obtención de datos
-   Almacenamiento de datos

El objetivo de la charla sería la descripción de las diferentes herramientas y software libre que se está usando en [galiciacorre.es](https://www.galiciacorre.es) para demostrar que no es necesario acudir a herramientas privativas para montarte algo funcional y usable.

Se describirán los componentes principales usados y se entrará en detalle en los más llamativos

## Público objetivo

Cualquier asistente en general, con un mínimo conocimiento del software libre y con interés en montarse algo por su cuenta o simplemenete que quiera escuchar a unos gallegos explicando qué es lo que han hecho.

## Ponente(s)

-   **Christian López** ([@christianlrcalo](https://twitter.com/christianlrcalo)): Drupal developer & lover. Cofundador de [galiciacorre.es](https://www.galiciacorre.es). Ingeniero Informático por la USC.
-   **Diego Carracedo** ([@D_Carracedo](https://twitter.com/d_carracedo)): Drupal developer & frontend developer. Cofundador de [galiciacorre.es](https://www.galiciacorre.es). Ingeniero Informático por la USC.

### Contacto(s)

-   **Christian López**: clopezrodriguez95 at gmail dot com
-   **Diego Carracedo**: dcarracedo at gmail dot com

## Comentarios

De ser posible, aumentar el tiempo de la charla a 30 minutos para entrar algo en detalle en alguno de los puntos.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
