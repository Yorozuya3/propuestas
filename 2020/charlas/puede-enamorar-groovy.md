---
layout: 2020/post
section: propuestas
category: talks
title: Lo que te puede enamorar de Groovy (si no lo ha hecho ya)
state: confirmed
---

Charla sobre conceptos prácticos de Groovy.

## Formato de la propuesta

-   [ ] &nbsp;Charla (25 minutos)
-   [x] &nbsp;Charla relámpago (10 minutos)

## Descripción

En esta charla se explicarán algunos conceptos prácticos de Groovy, que puedes probar hoy mismo y que te llevarán a comprender la potencia de este lenguaje.

En ella pretendo explicar de forma descriptiva los siguientes conceptos (entre otros):

-   GString: la libertad del string
-   Each: haciendo simple recorrer una lista
-   Find y FindAll
-   List y Map: nunca ha sido tan fácil crear estas estructuras
-   Descargar un XML o JSON remoto y navegarlo
-   HTTP POST con HTTPBuilder

## Público objetivo

Para gente que aún no conoce Groovy o que son sus primeros pasos.

## Ponente(s)

Mi nombre es **Miguel Rueda** y trabajo con desarrollador desde el 2010. Actualmente estoy cooperando en la creación de un libro tutorial <http://groovy-lang.gitlab.io/101-scripts/> en el que estamos creando un documento que recoja los conocimientos que tenemos acerca de la programación en Groovy. He dado una única charla en el Greach Conference 2018 en Madrid.

### Contacto(s)

-   **Miguel Rueda García**: miguel.rueda.garcia at gmail dom com

## Comentarios

El formato de la charla será la explicación de unos breves ejemplos donde se vea rápidamente lo expuesto durante la misma.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
