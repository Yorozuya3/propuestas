---
layout: 2020/post
section: propuestas
category: devrooms
title: Derechos Digitales, Privacidad en Internet y Seguridad Informática
state: confirmed
---

En un momento en el que las grandes compañías tecnológicas se frotan las manos pensando en todo el beneficio que podrán sacar de nuestros datos o el peligro que puede entrañar la tecnología más inocente del mundo si se le da usos con objetivos cuestionables (o peor, inconscientes), es más necesario que nunca disponer del conocimiento sobre cómo funcionan las tecnologías que está tan integradas en nuestro día a día que nos rodean de forma casi inapreciable. Hoy más que nunca debemos ser conscientes de la necesidad de proteger nuestra privacidad y nuestra soberanía digital: tenemos que saber **QUÉ** tenemos a nuestra disposición y **CÓMO** podemos actuar; y esto, sin duda alguna, es algo que viene de la mano del **Software Libre**.

El planteamiento del contenido será el siguiente:

* Nos gustaría encontrar personas interesadas en hablar sobre la importancia de los derechos digitales, la privacidad en Internet y la seguridad informática. Temas como *vigilancia masiva, anonimato, criptografía, hacktivismo, antropología social, censura, criptomonedas, domótica, reconocimiento facial, sistemas biométricos, voto electrónico… podrían tener cabida perfecta, pero además cualquier cosa similar también puede ser muy interesante.*
* **Las charlas pueden ser de cualquier área**. En ámbitos como este, la multidisciplinaridad es una ventaja y una necesidad a la vez, nos encantaría acoger también charlas relacionadas con la temática desde un punto de vista distinto, como puede ser el de personas dedicadas a *derecho, filosofía, política, arte o cualquier otra rama*. **¡Lánzate y cuéntanoslo!**
* Al ser un evento totalmente online, esta vez más que nunca: sea cual sea tu edad, género, etnia o condición social, seas quien seas, si te gusta la temática, da igual donde te encuentre geográficamente ahora mismo **¡ANÍMATE!**
* La única restricción es que todos los recursos en los que estén apoyados las charlas tienen que ser obligatoriamente de **licencia libre**.
* Las charlas podrán tener una duración de entre 10-20 minutos para que se adapten al esquema del congreso.
* Si la propuesta de charla es aceptada, las propuestas se podrán enviar a través de un formulario que se activará en [está página](https://interferencias.tech/eslibre2020/).

## Comunidad o grupo que lo propone

Desde **[Interferencias](https://interferencias.tech)** siempre nos gusta colaborar en iniciativas que nos parecen interesantes, y es por eso que tanto el [año pasado](https://eslib.re/2019/) como [este año](https://eslib.re/2020/) estamos colaborando en la organización de **esLibre**; no podemos más que brindar todo nuestro apoyo a quien quiera reivindicar la importancia del conocimiento y la difusión del las *tecnologías libres y cultura abierta*.  

Anualmente realizamos un evento propio para crear espacios donde las personas puedan expresar libremente sus inquietudes en materia de derechos digitales y seguridad informática, las **[Jornadas de Anonimato, Seguridad y Privacidad (JASYP)](https://interferencias.tech/jasyp)**, pero que este año no hemos podido realizar debido a la situación actual. Todos los años planteamos las Jornadas como una celebración donde compartir experiencias y conocimiento con la espontaneidad del trato en persona, es por eso que este año decidimos que no tendría sentido celebrarlas de forma online, se perdería su significado: *no se trata de ser otro congreso, se trata de ser una experiencia aprovechando todo lo que nos brinda Granada*.

Por todo esto que comentamos, finalmente hemos decidido presentar una **propuesta de sala** dentro del programa de **esLibre** para seguir llegando a tanta gente como sea posible.

### Contactos

-   **Paula de la Hoz**: @terceranexus6 Telegram/Gitlab
-   **Germán Martínez**: germaaan at interferencias dot tech

## Público objetivo

Todo el mundo.

## Tiempo

En principio, un día.

## Día

Indiferente.

## Formato

Se aceptarán charlas con una duración entre 10-20 minutos.

## Comentarios

Ninguno.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
-   [x] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
